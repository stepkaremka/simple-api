<?php

declare(strict_types = 1);

namespace App\Api;

use JsonSchema\Validator;
use function array_map;
use function is_scalar;
use function realpath;
use function sprintf;

abstract class BaseJsonSchemaValidator
{

    public function validateSinglePostRequest(array $requestData): array
    {
        return $this->validate((object) $requestData, $this->getSchema('singlePostRequest'));
    }

    public function validateBulkPostRequest(array $requestData): array
    {
        $data = [];
        foreach ($requestData as $row) {
            $data[] = is_scalar($row) ? $row : (object) $row;
        }
        return $this->validate($data, $this->getSchema('bulkPostRequest'));
    }

    /**
     * @param mixed $data
     * @return string[]
     */
    protected function validate($data, string $schema): array
    {
        $validator = new Validator();
        $validator->validate($data, (object) ['$ref' => $schema]);
        return $validator->isValid() ? [] : array_map(function (array $error): string {
            return $error['property'] === ''
                ? $error['message']
                : sprintf('`%s` error. %s.', $error['property'], $error['message']);
        }, $validator->getErrors());
    }

    protected function getSchema(string $filename): string
    {
        return sprintf('file://%s', realpath(sprintf('%s/schema/%s.json', $this->getBaseDir(), $filename)));
    }

    abstract protected function getBaseDir(): string;

}
