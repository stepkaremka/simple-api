<?php

declare(strict_types = 1);

namespace App\Api;

interface BulkPostRequestValidator
{

    public function validateBulkPostRequest(array $requestData): array;

}
