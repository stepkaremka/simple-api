<?php

declare(strict_types = 1);

namespace App\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

final class ResponseFactory
{

    public function createResponse(?array $data = null, int $statusCode = Response::HTTP_OK): JsonResponse
    {
        return new JsonResponse($data, $statusCode);
    }

    public function createErrorResponse(array $errors, int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR): JsonResponse
    {
        return new JsonResponse([
            'code' => $statusCode,
            'errors' => $errors,
        ], $statusCode);
    }

    public function createStreamableResponse(callable $callback): Response
    {
        $response = new StreamedResponse($callback);
        $response->headers->set('X-Accel-Buffering', 'no');

        return $response->send();
    }

}
