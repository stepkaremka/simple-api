<?php

declare(strict_types = 1);

namespace App\Api;

interface SinglePostRequestValidator
{

    public function validateSinglePostRequest(array $requestData): array;

}
