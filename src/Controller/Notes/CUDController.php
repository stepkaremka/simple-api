<?php

declare(strict_types = 1);

namespace App\Controller\Notes;

use App\Api\ResponseFactory;
use App\Model\Notes\Api\NoteResponseMapper;
use App\Model\Notes\CUD\Api\PostNoteRequestValidator;
use App\Model\Notes\CUD\CUDNoteException;
use App\Model\Notes\CUD\CreateNoteHandler;
use App\Model\Notes\CUD\DeleteNoteHandler;
use App\Model\Notes\CUD\PostNoteRequestFactory;
use App\Model\Notes\CUD\UpdateNoteHandler;
use App\Model\Notes\NoteNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/v1")
 */
class CUDController
{

    /** @var \App\Api\ResponseFactory */
    private $responseFactory;

    /** @var \App\Model\Notes\CUD\Api\PostNoteRequestValidator */
    private $requestValidator;

    /** @var \App\Model\Notes\CUD\PostNoteRequestFactory */
    private $requestFactory;

    /** @var \App\Model\Notes\CUD\CreateNoteHandler */
    private $createNoteHandler;

    /** @var \App\Model\Notes\CUD\UpdateNoteHandler */
    private $updateNoteHandler;

    /** @var \App\Model\Notes\CUD\DeleteNoteHandler */
    private $deleteNoteHandler;

    /** @var \App\Model\Notes\Api\NoteResponseMapper */
    private $noteResponseMapper;

    public function __construct(ResponseFactory $responseFactory, PostNoteRequestValidator $requestValidator, PostNoteRequestFactory $requestFactory, CreateNoteHandler $createNoteHandler, UpdateNoteHandler $updateNoteHandler, DeleteNoteHandler $deleteNoteHandler, NoteResponseMapper $noteResponseMapper)
    {
        $this->responseFactory = $responseFactory;
        $this->requestValidator = $requestValidator;
        $this->requestFactory = $requestFactory;
        $this->createNoteHandler = $createNoteHandler;
        $this->updateNoteHandler = $updateNoteHandler;
        $this->deleteNoteHandler = $deleteNoteHandler;
        $this->noteResponseMapper = $noteResponseMapper;
    }

    /**
     * @Route("/notes",
     *     methods={"POST"}
     * )
     */
    public function create(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            return $this->responseFactory->createErrorResponse(['Empty body'], Response::HTTP_BAD_REQUEST);
        }
        $errors = $this->requestValidator->validateSinglePostRequest($data);
        if (count($errors) > 0) {
            return $this->responseFactory->createErrorResponse($errors, Response::HTTP_BAD_REQUEST);
        }
        $noteRequest = $this->requestFactory->createFromApiData($data);
        try {
            $note = $this->createNoteHandler->handle($noteRequest);

            return $this->responseFactory->createResponse($this->noteResponseMapper->mapNoteToArray($note), Response::HTTP_CREATED);
        } catch (CUDNoteException $e) {
            return $this->responseFactory->createErrorResponse([$e->getMessage()]);
        }
    }

    /**
     * @Route("/notes/{id}",
     *     methods={"PUT"}
     * )
     */
    public function update(int $id, Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $errors = $this->requestValidator->validateSinglePostRequest($data);
        if (count($errors) > 0) {
            return $this->responseFactory->createErrorResponse($errors, Response::HTTP_BAD_REQUEST);
        }
        $noteRequest = $this->requestFactory->createFromApiData($data);
        try {
            $note = $this->updateNoteHandler->handle($id, $noteRequest);

            return $this->responseFactory->createResponse($this->noteResponseMapper->mapNoteToArray($note));
        } catch (NoteNotFoundException $e) {
            return $this->responseFactory->createErrorResponse([$e->getMessage()], Response::HTTP_NOT_FOUND);
        } catch (CUDNoteException $e) {
            return $this->responseFactory->createErrorResponse([$e->getMessage()]);
        }
    }

    /**
     * @Route("/notes/{id}",
     *     methods={"DELETE"}
     * )
     */
    public function delete(int $id): Response
    {
        try {
            $this->deleteNoteHandler->handle($id);

            return $this->responseFactory->createResponse();
        } catch (NoteNotFoundException $e) {
            return $this->responseFactory->createErrorResponse([$e->getMessage()], Response::HTTP_NOT_FOUND);
        } catch (CUDNoteException $e) {
            return $this->responseFactory->createErrorResponse([$e->getMessage()]);
        }
    }

}
