<?php

declare(strict_types = 1);

namespace App\Controller\Notes;

use App\Api\ResponseFactory;
use App\Model\Notes\Api\NoteResponseMapper;
use App\Model\Notes\Detail\DetailHandler;
use App\Model\Notes\NoteNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/v1")
 */
class DetailController
{

    /** @var \App\Api\ResponseFactory */
    private $responseFactory;

    /** @var \App\Model\Notes\Detail\DetailHandler */
    private $detailHandler;

    /** @var \App\Model\Notes\Api\NoteResponseMapper */
    private $noteResponseMapper;

    public function __construct(ResponseFactory $responseFactory, DetailHandler $detailHandler, NoteResponseMapper $noteResponseMapper)
    {
        $this->responseFactory = $responseFactory;
        $this->detailHandler = $detailHandler;
        $this->noteResponseMapper = $noteResponseMapper;
    }

    /**
     * @Route("/notes/{id}",
     *     methods={"GET"}
     * )
     */
    public function detail(int $id): Response
    {
        try {
            $note = $this->detailHandler->handle($id);

            return $this->responseFactory->createResponse($this->noteResponseMapper->mapNoteToArray($note));
        } catch (NoteNotFoundException $e) {
            return $this->responseFactory->createResponse(null, Response::HTTP_NOT_FOUND);
        }
    }

}
