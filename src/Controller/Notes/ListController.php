<?php

declare(strict_types = 1);

namespace App\Controller\Notes;

use App\Api\ResponseFactory;
use App\Model\Notes\Archive\ArchiveDataProvider;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/v1")
 */
class ListController
{

    /** @var \App\Api\ResponseFactory */
    private $responseFactory;

    /** @var \App\Model\Notes\Archive\ArchiveDataProvider */
    private $dataProvider;

    public function __construct(ResponseFactory $responseFactory, ArchiveDataProvider $dataProvider)
    {
        $this->responseFactory = $responseFactory;
        $this->dataProvider = $dataProvider;
    }

    /**
     * @Route("/notes",
     *     methods={"GET"}
     * )
     */
    public function archive(): Response
    {
        return $this->responseFactory->createResponse($this->dataProvider->loadData([]));
    }

}
