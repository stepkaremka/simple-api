<?php

declare(strict_types = 1);

namespace App\Model\Notes\Api;

class NoteApiSchema
{

    public const KEY_ID = 'id';
    public const KEY_TITLE = 'title';
    public const KEY_CONTENT = 'content';
    public const KEY_CREATED = 'created';
    public const KEY_UPDATED = 'updated';

    public const DATE_FORMAT = DATE_ATOM;

}
