<?php

declare(strict_types = 1);

namespace App\Model\Notes\Api;

use App\Model\Notes\Note\Note;

class NoteResponseMapper
{

    public function mapNoteToArray(Note $note): array
    {
        $updated = $note->getUpdated() === null ? null : $note->getUpdated()->format(NoteApiSchema::DATE_FORMAT);

        return [
            NoteApiSchema::KEY_ID => $note->getId(),
            NoteApiSchema::KEY_TITLE => $note->getTitle(),
            NoteApiSchema::KEY_CONTENT => $note->getContent(),
            NoteApiSchema::KEY_CREATED => $note->getCreated()->format(NoteApiSchema::DATE_FORMAT),
            NoteApiSchema::KEY_UPDATED => $updated,
        ];
    }

    public function mapDbArrayToArray(array $data): array
    {
        $updated = $data['updated'] === null ? null : $data['updated']->format(NoteApiSchema::DATE_FORMAT);

        return [
            NoteApiSchema::KEY_ID => $data['id'],
            NoteApiSchema::KEY_TITLE => $data['title'],
            NoteApiSchema::KEY_CONTENT => $data['content'],
            NoteApiSchema::KEY_CREATED => $data['created']->format(NoteApiSchema::DATE_FORMAT),
            NoteApiSchema::KEY_UPDATED => $updated,
        ];
    }

}
