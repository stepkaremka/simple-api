<?php

declare(strict_types = 1);

namespace App\Model\Notes\Archive;

use App\Model\Notes\Api\NoteResponseMapper;
use App\Model\Notes\Note\Note;
use Doctrine\ORM\EntityManagerInterface;

class ArchiveDataProvider
{

    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $entityManager;

    /** @var \App\Model\Notes\Api\NoteResponseMapper */
    private $noteResponseMapper;

    public function __construct(EntityManagerInterface $entityManager, NoteResponseMapper $noteResponseMapper)
    {
        $this->entityManager = $entityManager;
        $this->noteResponseMapper = $noteResponseMapper;
    }

    public function loadData(array $filter): array
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('n.id, n.title, n.content, n.created, n.updated')
            ->from(Note::class, 'n');
        // apply filters

        $result = $qb->getQuery()->getArrayResult();
        array_walk($result, function (&$item, $key): void {
            $item = $this->noteResponseMapper->mapDbArrayToArray($item);
        });

        return $result;
    }

}
