<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD\Api;

use App\Api\BaseJsonSchemaValidator;

final class JsonSchemaPostNoteRequestValidator extends BaseJsonSchemaValidator implements PostNoteRequestValidator
{

    protected function getBaseDir(): string
    {
        return __DIR__;
    }

}
