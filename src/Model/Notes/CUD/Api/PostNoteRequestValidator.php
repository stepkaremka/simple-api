<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD\Api;

use App\Api\SinglePostRequestValidator;

interface PostNoteRequestValidator extends SinglePostRequestValidator
{

}
