<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD;

class CUDNoteException extends \Exception
{

    public static function dbError(string $message, \Throwable $previous): self
    {
        return new self($message, 0, $previous);
    }

}
