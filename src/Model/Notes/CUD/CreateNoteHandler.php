<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD;

use App\Model\Notes\Note\Note;

interface CreateNoteHandler
{

    public function handle(PostNoteRequest $request): Note;

}
