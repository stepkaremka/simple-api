<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD;

use App\Model\Notes\Note\Note;
use App\Model\Notes\Note\NoteRepository;
use Doctrine\ORM\ORMException;

final class DefaultCreateNoteHandler implements CreateNoteHandler
{

    /** @var \App\Model\Notes\Note\NoteRepository */
    private $noteRepository;

    public function __construct(NoteRepository $noteRepository)
    {
        $this->noteRepository = $noteRepository;
    }

    public function handle(PostNoteRequest $request): Note
    {
        try {
            return $this->noteRepository->create($request->getTitle(), $request->getContent());
        } catch (ORMException $e) {
            throw CUDNoteException::dbError(sprintf('Error while creating note: %s', $e->getMessage()), $e);
        }
    }

}
