<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD;

use App\Model\Notes\Note\NoteRepository;
use App\Model\Notes\NoteNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;

final class DefaultDeleteNoteHandler implements DeleteNoteHandler
{

    /** @var \App\Model\Notes\Note\NoteRepository */
    private $noteRepository;

    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $entityManager;

    public function __construct(NoteRepository $noteRepository, EntityManagerInterface $entityManager)
    {
        $this->noteRepository = $noteRepository;
        $this->entityManager = $entityManager;
    }

    public function handle(int $id): void
    {
        $note = $this->noteRepository->findById($id);
        if ($note === null) {
            throw NoteNotFoundException::notFound($id);
        }
        try {
            $this->entityManager->remove($note);
            $this->entityManager->flush();
        } catch (ORMException $e) {
            throw CUDNoteException::dbError(sprintf('Error while updating note #%d. (%s)', $id, $e->getMessage()), $e);
        }
    }

}
