<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD;

interface DeleteNoteHandler
{

    public function handle(int $id): void;

}
