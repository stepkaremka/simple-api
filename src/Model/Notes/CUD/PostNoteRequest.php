<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD;

class PostNoteRequest
{

    /** @var string */
    private $title;

    /** @var string */
    private $content;

    public function __construct(string $title, string $content)
    {
        $this->title = $title;
        $this->content = $content;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

}
