<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD;

use App\Model\Notes\Api\NoteApiSchema;

class PostNoteRequestFactory
{

    public function createFromApiData(array $data): PostNoteRequest
    {
        $title = (string) $data[NoteApiSchema::KEY_TITLE];
        $content = (string) $data[NoteApiSchema::KEY_CONTENT];

        return new PostNoteRequest($title, $content);
    }

}
