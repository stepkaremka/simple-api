<?php

declare(strict_types = 1);

namespace App\Model\Notes\CUD;

use App\Model\Notes\Note\Note;

interface UpdateNoteHandler
{

    public function handle(int $id, PostNoteRequest $request): Note;

}
