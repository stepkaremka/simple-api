<?php

declare(strict_types = 1);

namespace App\Model\Notes\Detail;

use App\Model\Notes\Note\Note;
use App\Model\Notes\Note\NoteRepository;
use App\Model\Notes\NoteNotFoundException;

final class DefaultDetailHandler implements DetailHandler
{

    /** @var \App\Model\Notes\Note\NoteRepository */
    private $noteRepository;

    public function __construct(NoteRepository $noteRepository)
    {
        $this->noteRepository = $noteRepository;
    }

    public function handle(int $id): Note
    {
        $note = $this->noteRepository->findById($id);
        if ($note === null) {
            throw NoteNotFoundException::notFound($id);
        }

        return $note;
    }

}
