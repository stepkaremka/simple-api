<?php

declare(strict_types = 1);

namespace App\Model\Notes\Detail;

use App\Model\Notes\Note\Note;

interface DetailHandler
{

    public function handle(int $id): Note;

}
