<?php

declare(strict_types = 1);

namespace App\Model\Notes\Note;

use Doctrine\ORM\EntityRepository;

final class DefaultNoteRepository extends EntityRepository implements NoteRepository
{

    public function create(string $title, string $content): Note
    {
        $entity = new Note($title, $content);
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);

        return $entity;
    }

    public function findById(int $id): ?Note
    {
        $object = $this->find($id);

        return $object instanceof Note ? $object : null;
    }

}
