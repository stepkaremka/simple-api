<?php

declare(strict_types = 1);

namespace App\Model\Notes\Note;

interface NoteRepository
{

    public function create(string $title, string $content): Note;

    public function findById(int $id): ?Note;

}
