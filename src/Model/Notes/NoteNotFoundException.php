<?php

declare(strict_types = 1);

namespace App\Model\Notes;

class NoteNotFoundException extends \Exception
{

    public static function notFound(int $id): self
    {
        return new self(sprintf('Note #%d not found', $id));
    }

}
