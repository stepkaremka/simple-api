<?php

declare(strict_types = 1);

namespace App;

use ReflectionClass;

final class Helper
{

    /**
     * @param mixed $object
     * @param mixed $name
     * @param mixed $value
     */
    public static function setProperty($object, $name, $value): void
    {
        $reflection = new ReflectionClass($object);
        $reflectionProperty = $reflection->getProperty($name);
        $origIsPublic = $reflectionProperty->isPublic();
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($object, $value);
        $reflectionProperty->setAccessible($origIsPublic);
    }

}
