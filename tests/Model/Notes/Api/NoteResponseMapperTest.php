<?php

declare(strict_types = 1);

namespace App\Model\Notes\Api;

use App\Helper;
use App\Model\Notes\Note\Note;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Model\Notes\Api\NoteResponseMapper
 */
final class NoteResponseMapperTest extends TestCase
{

    /**
     * @dataProvider provideMapNoteToArrayData
     */
    public function testMapNoteToArray(Note $note, array $expectedArray): void
    {
        $mapper = new NoteResponseMapper();
        self::assertSame($expectedArray, $mapper->mapNoteToArray($note));
    }

    public function provideMapNoteToArrayData(): array
    {
        $createdNote = new Note('title', 'content');
        Helper::setProperty($createdNote, 'id', 2);
        $createdNoteArray = $this->responseArray(2, 'title', 'content', $createdNote->getCreated());

        $updatedNote = new Note('tit', 'con');
        Helper::setProperty($updatedNote, 'id', 3);
        $updatedNote->update('tit', 'con');
        $updatedNoteArray = $this->responseArray(3, 'tit', 'con', $updatedNote->getCreated(), $updatedNote->getUpdated());

        return [
            [$createdNote, $createdNoteArray],
            [$updatedNote, $updatedNoteArray],
        ];
    }

    /**
     * @dataProvider provideMapDbArrayToArrayData
     */
    public function testMapDbArrayToArray(array $dbArray, array $expectedArray): void
    {
        $mapper = new NoteResponseMapper();
        self::assertSame($expectedArray, $mapper->mapDbArrayToArray($dbArray));
    }

    public function provideMapDbArrayToArrayData(): array
    {
        $created = new DateTimeImmutable();
        $updated = new DateTime();
        $updated->modify('+1day');
        $createdDb = ['id' => 1, 'title' => '2', 'content' => '3', 'created' => $created, 'updated' => null];
        $createdRes = $this->responseArray(1, '2', '3', $created);
        $updatedDb = ['id' => 2, 'title' => '3', 'content' => '4', 'created' => $created, 'updated' => $updated];
        $updatedRes = $this->responseArray(2, '3', '4', $created, $updated);

        return [
            [$createdDb, $createdRes],
            [$updatedDb, $updatedRes],
        ];
    }

    private function responseArray(int $id, string $title, string $content, DateTimeInterface $created, ?DateTimeInterface $updated = null): array
    {
        $updatedFormatted = $updated === null ? null : $updated->format(NoteApiSchema::DATE_FORMAT);

        return [
            NoteApiSchema::KEY_ID => $id,
            NoteApiSchema::KEY_TITLE => $title,
            NoteApiSchema::KEY_CONTENT => $content,
            NoteApiSchema::KEY_CREATED => $created->format(NoteApiSchema::DATE_FORMAT),
            NoteApiSchema::KEY_UPDATED => $updatedFormatted,
        ];
    }

}
